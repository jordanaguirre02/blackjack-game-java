package BlackJack;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.Map.entry;
import java.util.ArrayList;
import java.util.Collections;

public class Chips {
    Map<Integer,String> chipValue = new HashMap<>(Map.ofEntries(
        entry(100000, "gold"),
        entry(2500, "light-blue"),
        entry(1000, "yellow"),
        entry(500, "purple"),
        entry(250, "pink"),
        entry(100,"black"),
        entry(50, "orange"),
        entry(25, "green"),
        entry(10, "blue"),
        entry(5, "red"),
        entry(1, "white")
    ));
    List<Integer> chipKeys= new ArrayList<>(chipValue.keySet());
    int chipTotal;
    int betPlaced;
    int winTotal;
    int betSetting;
    ArrayList<Integer> chipStack;

    public void startchips(String mode) {
        betPlaced = 0;
        winTotal = 0;
        if (mode == "easy") {
            chipTotal = 10000;
        } else if (mode == "normal") {
            chipTotal = 5000;
        } else if (mode == "hard") {
            chipTotal = 2500;
        }
    }


    public void findChip() {
        chipStack = new ArrayList<>();
        int total = 0;
        Collections.sort(chipKeys, Collections.reverseOrder());
        for (int key: chipKeys) {
            while (key + total <= chipTotal) {
                total += key;
                chipStack.add(key);
            }

        }
    }

    public void setBet(int bet) {
        if (betSetting + bet > 500) {
            betSetting = 500;
        } else if (bet == 0) {
            betSetting = 0;
        } else {
            betSetting += bet;
        }
    }

    public void placeBet() {
        betPlaced = betSetting;
    }

    public void findWinnings(boolean blackJack) {
        if (blackJack) {
            double temp = betPlaced * 2.5;
            winTotal = (int)temp;
        } else {
            winTotal = betPlaced * 2;
        }
    }

    public void pushWinnings() {
        winTotal = betPlaced;
    }

    public void subtractBet() {
        chipTotal -= betPlaced;
    }

    public void addWinnings() {
        chipTotal += winTotal;
        winTotal = 0;
    }


}
