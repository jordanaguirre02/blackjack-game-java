package BlackJack;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;


public class Deck {
    ArrayList<String> deck1;
    HashMap<String, Integer> cardMap;
    HashMap<String,String> cardImages;
    LinkedList<String> mainDeck;
    LinkedList<String> burnPile;
    LinkedList<String> trueBurnPile;
    LinkedList<String> shuffledDeck;
    ArrayList<String> playerHand;
    ArrayList<String> dealerHand;
    String currentCard;
    String dealersCurrentCard;
    String burnerCard;
    int dealerHandTotal;
    int handTotal;
    int softCount;
    double realSoftCount;
    double trueCount;


    public void createDeck() {
        burnPile = new LinkedList<String>();
        trueBurnPile = new LinkedList<String>();
        dealerHand = new ArrayList<String>();
        playerHand = new ArrayList<String>();
        cardMap = new HashMap<String, Integer>();
        cardImages = new HashMap<String,String>();
        deck1 = new ArrayList<String>();
        String[] cardValue = {
            "Ace", "Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King"
        };

        for (int i = 0; i < 13; i++) {
            String diamonds = "%sOfDiamonds".formatted(cardValue[i]);
            String hearts = "%sOfHearts".formatted(cardValue[i]);
            String clubs = "%sOfClubs".formatted(cardValue[i]);
            String spades = "%sOfSpades".formatted(cardValue[i]);

            deck1.add(diamonds);
            deck1.add(hearts);
            deck1.add(clubs);
            deck1.add(spades);

            cardImages.put(diamonds, "images/cards/%s.png".formatted(diamonds));
            cardImages.put(hearts, "images/cards/%s.png".formatted(hearts));
            cardImages.put(clubs, "images/cards/%s.png".formatted(clubs));
            cardImages.put(spades, "images/cards/%s.png".formatted(spades));

            if (cardValue[i].equals(
                "Jack") ||
                cardValue[i].equals("Queen") ||
                cardValue[i].equals("King")
                ) {
                cardMap.put(diamonds, 10);
                cardMap.put(hearts, 10);
                cardMap.put(clubs, 10);
                cardMap.put(spades, 10);
            } else {
                cardMap.put(diamonds, i+1);
                cardMap.put(hearts, i+1);
                cardMap.put(clubs, i+1);
                cardMap.put(spades, i+1);
            }


        }
    }

    public void createMainDeck() {
        mainDeck = new LinkedList<String>();
        //add 6 decks of cards to the main deck
        //1
        mainDeck.addAll(deck1);
        //2
        mainDeck.addAll(deck1);
        //3
        mainDeck.addAll(deck1);
        //4
        mainDeck.addAll(deck1);
        //5
        mainDeck.addAll(deck1);
        //6
        mainDeck.addAll(deck1);

    }

    public void shuffleDeck() {
        shuffledDeck = new LinkedList<String>();
        for (int i = 312; i > 0; i--) {
            Random shuffleCard = new Random();
            int num = shuffleCard.nextInt(i);
            shuffledDeck.add(mainDeck.get(num));
            mainDeck.remove(num);
        }
    }

    public void burnCount() {
        softCount = 0;
        realSoftCount = 0;
        trueCount = 0;
        for (int i = 0; i < burnPile.size(); i++) {
            String checkCard = burnPile.get(i);
            int checkValue = cardMap.get(checkCard);
            if (checkValue < 7 && checkValue > 1) {
                softCount += 1;
            } else if (checkValue == 10 || checkValue == 1) {
                softCount -= 1;
            }
        }
        for (int i = 0; i < trueBurnPile.size(); i++) {
            String tempCard = trueBurnPile.get(i);
            int tempValue = cardMap.get(tempCard);
            if (tempValue < 7 && tempValue > 1) {
                realSoftCount += 1;
            } else if (tempValue == 10 || tempValue == 1) {
                realSoftCount -= 1;
            }
        }
        double decksLeft = shuffledDeck.size() / 52;
        trueCount = (realSoftCount / decksLeft);
    }

    public void burnCard() {
        burnerCard = shuffledDeck.get(0);
        trueBurnPile.add(burnerCard);
        burnCount();
        shuffledDeck.remove(0);
    }

    public void dealerAddBurn() {
        burnPile.add(dealersCurrentCard);
        burnCount();
    }

    public void dealerCards() {
        dealersCurrentCard = shuffledDeck.get(0);
        trueBurnPile.add(dealersCurrentCard);
        dealerHand.add(dealersCurrentCard);
        shuffledDeck.remove(0);
    }

    public void dealCard() {
        currentCard = shuffledDeck.get(0);
        playerHand.add(currentCard);
        burnPile.add(currentCard);
        trueBurnPile.add(currentCard);
        burnCount();
        shuffledDeck.remove(0);
        }

    public void newDeal() {
        playerHand.clear();
        dealerHand.clear();
    }

    public void handCalc() {
        boolean hasAce = false;
        handTotal = 0;
        for (int i = 0; i < playerHand.size(); i++) {
            String cardCheck = playerHand.get(i);
            if (cardCheck.contains("Ace")) {
                hasAce = true;
            }
                handTotal += cardMap.get(cardCheck);
        }
        if (hasAce && handTotal <= 11) {
            handTotal += 10;
        }
    }

    public void dealerHandCalc() {
        boolean hasAce = false;
        dealerHandTotal = 0;
        for (int i = 0; i < dealerHand.size(); i++) {
            String cardCheck = dealerHand.get(i);
            if (cardCheck.contains("Ace")) {
                hasAce = true;
            }
                dealerHandTotal += cardMap.get(cardCheck);
        }
        if (hasAce && dealerHandTotal <= 11) {
            dealerHandTotal += 10;
        }
    }

}
