package BlackJack;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;



public class CCFrame extends JFrame implements ActionListener{
    JButton buttonExtras;
    JButton buttonStart;
    JButton buttonEasy;
    JButton buttonNormal;
    JButton buttonHard;
    JButton hit;
    JButton stand;
    JButton newHand;

    JButton betFive;
    JButton betQuarter;
    JButton betFifty;
    JButton betTfifty;
    JButton betReset;

    JLabel startScreen;
    JLabel mainBackImage;

    JLabel newCard;
    JLabel newCard2;
    JLabel newCard3;
    JLabel newCard4;
    JLabel newCard5;
    JLabel newCard6;

    JLabel deckStack;
    JLabel countLabel;
    JLabel trueCountLabel;
    JLabel chipLabel;
    JLabel betLabel;
    JLabel winningsLabel;
    JLabel totalLabel;
    JLabel winnerLabel;
    JLabel loserLabel;
    JLabel blackjackLabel;
    JLabel pushLabel;

    JLabel dealerCard1;
    JLabel dealerCard2;
    JLabel dealerCard3;
    JLabel dealerCard4;
    JLabel dealerCard5;
    JLabel dealerCard6;
    JLabel dealerTotal;

    JPanel backContainer;

    Deck obj;
    ImageIcon dealerSecond;
    Chips player;
    Chips cpuApple;
    Chips cpuBanano;

    CCFrame(){
    }


    public void startFrame() {
        obj = new Deck();
        player = new Chips();
        cpuApple = new Chips();
        cpuBanano = new Chips();

        this.setTitle("Card Counters Deluxe");
        this.setSize(512, 512);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.getContentPane();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        ImageIcon icon = new ImageIcon(getClass().getResource("images/icon.png"));
        this.setIconImage(icon.getImage());
    }

    public void StartBackground() {
        startScreen = new JLabel();
        startScreen.setLayout(null);
        startScreen.setSize(512, 512);
        ImageIcon homeScreen = new ImageIcon(getClass().getResource("images/home.png"));
        startScreen.setIcon(homeScreen);
        startScreen.setVisible(true);
        backContainer = new JPanel();
        backContainer.setLayout(null);
        backContainer.add(startScreen);
        backContainer.setVisible(true);
        this.add(backContainer);
        this.setVisible(true);
    }

    public void StartButtons() {
        buttonStart = new JButton();
        buttonStart.setBounds(206, 120, 100, 50);
        buttonStart.setText("Start");
        buttonStart.setBackground(Color.green);
        buttonStart.setFont(new Font("Vivaldi", Font.BOLD, 24));
        buttonStart.addActionListener(this);
        buttonStart.setVisible(true);

        buttonExtras = new JButton();
        buttonExtras.setBounds(206, 200, 100, 50);
        buttonExtras.setText("Extras");
        buttonExtras.setBackground(Color.green);
        buttonExtras.setFont(new Font("Vivaldi", Font.BOLD, 22));
        buttonExtras.addActionListener(this);
        buttonExtras.setVisible(true);

        startScreen.add(buttonStart);
        startScreen.add(buttonExtras);
        startScreen.setComponentZOrder(buttonStart, 0);
        startScreen.setComponentZOrder(buttonExtras, 1);
        startScreen.setVisible(true);
        backContainer.repaint();
    }

    public void difficulty() {
        buttonEasy = new JButton();
        buttonEasy.setBounds(101, 120, 100, 50);
        buttonEasy.setText("Easy");
        buttonEasy.setBackground(Color.green);
        buttonEasy.setFont(new Font("Vivaldi", Font.BOLD, 24));
        buttonEasy.addActionListener(this);
        buttonEasy.setVisible(true);

        buttonNormal = new JButton();
        buttonNormal.setBounds(206, 120, 120, 50);
        buttonNormal.setText("Normal");
        buttonNormal.setBackground(Color.green);
        buttonNormal.setFont(new Font("Vivaldi", Font.BOLD, 22));
        buttonNormal.addActionListener(this);
        buttonNormal.setVisible(true);

        buttonHard = new JButton();
        buttonHard.setBounds(331, 120, 100, 50);
        buttonHard.setText("Hard");
        buttonHard.setBackground(Color.green);
        buttonHard.setFont(new Font("Vivaldi", Font.BOLD, 22));
        buttonHard.addActionListener(this);
        buttonHard.setVisible(true);

        startScreen.removeAll();
        startScreen.add(buttonEasy);
        startScreen.add(buttonNormal);
        startScreen.add(buttonHard);
        startScreen.repaint();
    }

    public void createLabels() {
        deckStack = new JLabel();
        ImageIcon stackIcon = new ImageIcon(getClass().getResource("images/cardStack.png"));
        deckStack.setLayout(null);
        deckStack.setBounds(550, 50, 330, 139);
        deckStack.setOpaque(true);
        deckStack.setIcon(stackIcon);
        deckStack.setVisible(true);

        countLabel = new JLabel();
        countLabel.setLayout(null);
        countLabel.setBounds(100, 50, 103, 30);
        countLabel.setBackground(Color.green);
        countLabel.setOpaque(true);
        countLabel.setText("Soft Count is: 0");
        countLabel.setVisible(true);

        winnerLabel = new JLabel();
        ImageIcon winnerIcon = new ImageIcon(getClass().getResource("images/winnerLabel.png"));
        winnerLabel.setLayout(null);
        winnerLabel.setBounds(350, 220, 200, 75);
        winnerLabel.setIcon(winnerIcon);
        winnerLabel.setVisible(false);


        loserLabel = new JLabel();
        ImageIcon loserIcon = new ImageIcon(getClass().getResource("images/loseLabel.png"));
        loserLabel.setLayout(null);
        loserLabel.setBounds(350, 220, 200, 75);
        loserLabel.setIcon(loserIcon);
        loserLabel.setVisible(false);


        blackjackLabel = new JLabel();
        ImageIcon blackjackIcon = new ImageIcon(getClass().getResource("images/chickenDinner.png"));
        blackjackLabel.setLayout(null);
        blackjackLabel.setBounds(350, 220, 200, 75);
        blackjackLabel.setIcon(blackjackIcon);
        blackjackLabel.setVisible(false);

        pushLabel = new JLabel();
        ImageIcon pushIcon = new ImageIcon(getClass().getResource("images/pushLabel.png"));
        pushLabel.setLayout(null);
        pushLabel.setBounds(350, 220, 200, 75);
        pushLabel.setIcon(pushIcon);
        pushLabel.setVisible(false);

        betLabel = new JLabel();
        betLabel.setLayout(null);
        betLabel.setBounds(60, 380, 300, 35);
        betLabel.setText("Current Bet: 5");
        betLabel.setVerticalTextPosition(JLabel.CENTER);
        betLabel.setFont(new Font("Serif", Font.BOLD, 30));
        betLabel.setBackground(Color.LIGHT_GRAY);
        betLabel.setOpaque(true);
        betLabel.setVisible(true);

        trueCountLabel = new JLabel();
        trueCountLabel.setLayout(null);
        trueCountLabel.setBounds(215, 50, 130, 30);
        trueCountLabel.setBackground(Color.green);
        trueCountLabel.setOpaque(true);
        trueCountLabel.setText("True Count is: 0");
        trueCountLabel.setVisible(true);

        winningsLabel = new JLabel();
        winningsLabel.setLayout(null);
        winningsLabel.setBounds(100, 340, 220, 35);
        winningsLabel.setText("You Won: 0!");
        winningsLabel.setVerticalTextPosition(JLabel.CENTER);
        winningsLabel.setFont(new Font("Serif", Font.BOLD, 30));
        winningsLabel.setBackground(Color.RED);
        winningsLabel.setOpaque(true);
        winningsLabel.setVisible(false);

        chipLabel = new JLabel();
        ImageIcon chipIcon = new ImageIcon(getClass().getResource("images/chipTotalStack.png"));
        chipLabel.setLayout(null);
        chipLabel.setBounds(60, 500, 300, 125);
        chipLabel.setIcon(chipIcon);
        chipLabel.setText("%s".formatted(player.chipTotal));
        chipLabel.setVerticalTextPosition(JLabel.CENTER);
        chipLabel.setHorizontalTextPosition(JLabel.CENTER);
        chipLabel.setForeground(Color.CYAN);
        chipLabel.setFont(new Font("Serif", Font.BOLD, 70));
        chipLabel.setVisible(true);


        totalLabel = new JLabel();
        totalLabel.setLayout(null);
        totalLabel.setBounds(400, 370, 100, 30);
        totalLabel.setBackground(Color.green);
        totalLabel.setOpaque(true);
        totalLabel.setText("Hand Total: 0");
        totalLabel.setVisible(true);

        dealerCard1 = new JLabel();
        dealerCard1.setLayout(null);
        dealerCard1.setBounds(580, 210, 100, 139);
        dealerCard1.setOpaque(true);
        dealerCard1.setVisible(false);

        dealerCard2 = new JLabel();
        dealerCard2.setLayout(null);
        dealerCard2.setBounds(610, 210, 100, 139);
        dealerCard2.setOpaque(true);
        dealerCard2.setVisible(false);

        dealerCard3 = new JLabel();
        dealerCard3.setLayout(null);
        dealerCard3.setBounds(640, 210, 100, 139);
        dealerCard3.setOpaque(true);
        dealerCard3.setVisible(false);

        dealerCard4 = new JLabel();
        dealerCard4.setLayout(null);
        dealerCard4.setBounds(670, 210, 100, 139);
        dealerCard4.setOpaque(true);
        dealerCard4.setVisible(false);

        dealerCard5 = new JLabel();
        dealerCard5.setLayout(null);
        dealerCard5.setBounds(700, 210, 100, 139);
        dealerCard5.setOpaque(true);
        dealerCard5.setVisible(false);

        dealerCard6 = new JLabel();
        dealerCard6.setLayout(null);
        dealerCard6.setBounds(730, 210, 100, 139);
        dealerCard6.setOpaque(true);
        dealerCard6.setVisible(false);

        dealerTotal = new JLabel();
        dealerTotal.setLayout(null);
        dealerTotal.setBounds(400, 140, 100, 30);
        dealerTotal.setBackground(Color.green);
        dealerTotal.setOpaque(true);
        dealerTotal.setText("Dealer total: 0");
        dealerTotal.setVisible(false);
    }

    public void createMain() {
        newCard = new JLabel();
        newCard2 = new JLabel();
        newCard3 = new JLabel();
        newCard4 = new JLabel();
        newCard5 = new JLabel();
        newCard6 = new JLabel();

        newCard.setLayout(null);
        newCard.setBounds(550, 430, 100, 139);
        newCard.setOpaque(true);
        newCard.setVisible(false);

        newCard2.setLayout(null);
        newCard2.setBounds(580, 430, 100, 139);
        newCard2.setOpaque(true);
        newCard2.setVisible(false);

        newCard3.setLayout(null);
        newCard3.setBounds(610, 430, 100, 139);
        newCard3.setOpaque(true);
        newCard3.setVisible(false);

        newCard4.setLayout(null);
        newCard4.setBounds(640, 430, 100, 139);
        newCard4.setOpaque(true);
        newCard4.setVisible(false);

        newCard5.setLayout(null);
        newCard5.setBounds(670, 430, 100, 139);
        newCard5.setOpaque(true);
        newCard5.setVisible(false);

        newCard6.setLayout(null);
        newCard6.setBounds(700, 430, 100, 139);
        newCard6.setOpaque(true);
        newCard6.setVisible(false);
    }

    public void cardButtons() {
        hit = new JButton();
        stand = new JButton();
        newHand = new JButton();

        hit.setBounds(410, 430, 100, 50);
        hit.setText("Hit");
        hit.setBackground(Color.green);
        hit.setFont(new Font("Serif", Font.BOLD, 22));
        hit.addActionListener(this);
        hit.setVisible(true);

        stand.setBounds(410, 490, 100, 50);
        stand.setText("Stand");
        stand.setBackground(Color.green);
        stand.setFont(new Font("Serif", Font.BOLD, 22));
        stand.addActionListener(this);
        stand.setVisible(true);

        newHand.setBounds(540, 370, 150, 50);
        newHand.setText("New hand?");
        newHand.setBackground(Color.green);
        newHand.setFont(new Font("Serif", Font.BOLD, 22));
        newHand.addActionListener(this);
        newHand.setVisible(false);


    }

    public void betAdd(int num) {
        player.setBet(num);
        betLabel.setText("Current Bet: %s".formatted(player.betSetting));
    }

    public void betButtons() {
        betFive = new JButton();
        betFive.setBounds(60, 460, 50, 35);
        betFive.setText("5");
        betFive.setBackground(Color.GRAY);
        betFive.setFont(new Font("Serif", Font.BOLD, 22));
        betFive.addActionListener(this);
        betFive.setVisible(true);

        betQuarter = new JButton();
        betQuarter.setBounds(117, 460, 70, 35);
        betQuarter.setText("25");
        betQuarter.setBackground(Color.GRAY);
        betQuarter.setFont(new Font("Serif", Font.BOLD, 22));
        betQuarter.addActionListener(this);
        betQuarter.setVisible(true);

        betFifty = new JButton();
        betFifty.setBounds(193, 460, 70, 35);
        betFifty.setText("50");
        betFifty.setBackground(Color.GRAY);
        betFifty.setFont(new Font("Serif", Font.BOLD, 22));
        betFifty.addActionListener(this);
        betFifty.setVisible(true);

        betTfifty = new JButton();
        betTfifty.setBounds(270, 460, 90, 35);
        betTfifty.setText("250");
        betTfifty.setBackground(Color.GRAY);
        betTfifty.setFont(new Font("Serif", Font.BOLD, 22));
        betTfifty.addActionListener(this);
        betTfifty.setVisible(true);

        betReset = new JButton();
        betReset.setBounds(60, 420, 300, 35);
        betReset.setText("Reset");
        betReset.setBackground(Color.GRAY);
        betReset.setFont(new Font("Serif", Font.BOLD, 22));
        betReset.addActionListener(this);
        betReset.setVisible(true);
    }

    public void endHand() {
        hit.setVisible(false);
        stand.setVisible(false);
        newHand.setVisible(true);
    }

    public void checkBlackJack() {
        if (obj.handTotal == 21 && obj.playerHand.size() == 2) {
            player.findWinnings(true);
            winningsLabel.setVisible(true);
            winningsLabel.setText("You Won: %s!".formatted(player.winTotal));
            player.addWinnings();
            chipLabel.setText("%s".formatted(player.chipTotal));
            blackjackLabel.setVisible(true);
            endHand();
        }
    }

    public void checkWinner() {
        if (obj.handTotal == obj.dealerHandTotal) {
            player.pushWinnings();
            winningsLabel.setVisible(true);
            winningsLabel.setText("You Won: %s!".formatted(player.winTotal));
            player.addWinnings();
            chipLabel.setText("%s".formatted(player.chipTotal));
            pushLabel.setVisible(true);
        } else if (obj.handTotal > obj.dealerHandTotal || obj.dealerHandTotal > 21) {
            player.findWinnings(false);
            winningsLabel.setVisible(true);
            winningsLabel.setText("You Won: %s!".formatted(player.winTotal));
            player.addWinnings();
            chipLabel.setText("%s".formatted(player.chipTotal));
            winnerLabel.setVisible(true);
        } else {
            loserLabel.setVisible(true);
        }
    }

    public void setCounts() {
        trueCountLabel.setText("True Count is: %s".formatted(obj.trueCount));
        countLabel.setText("Soft Count is: %s".formatted(obj.softCount));
    }


    public void turnDealer() {
        dealerCard2.setIcon(dealerSecond);
        obj.dealerAddBurn();
        obj.dealerHandCalc();

        setCounts();
        dealerTotal.setText("Dealer total: %s".formatted(obj.dealerHandTotal));
        while (obj.dealerHandTotal < 17) {
            obj.dealerCards();
            obj.dealerAddBurn();
            obj.dealerHandCalc();
            setCounts();
            dealerTotal.setText("Dealer total: %s".formatted(obj.dealerHandTotal));
            ImageIcon hitDeal = new ImageIcon(getClass().getResource("images/cards/%s.png".formatted(obj.dealersCurrentCard)));
            if (dealerCard3.getIcon() == null) {
                dealerCard3.setIcon(hitDeal);
                dealerCard3.setVisible(true);
            } else if (dealerCard4.getIcon() == null) {
                dealerCard4.setIcon(hitDeal);
                dealerCard4.setVisible(true);
            } else if (dealerCard5.getIcon() == null) {
                dealerCard5.setIcon(hitDeal);
                dealerCard5.setVisible(true);
            } else if (dealerCard6.getIcon() == null) {
                dealerCard6.setIcon(hitDeal);
                dealerCard6.setVisible(true);
            }
        }
        checkWinner();
    }

    public void clearTable() {
        hit.setVisible(true);
        stand.setVisible(true);
        totalLabel.setVisible(true);
        dealerTotal.setVisible(true);
        chipLabel.setVisible(true);
        newHand.setVisible(false);
        winnerLabel.setVisible(false);
        loserLabel.setVisible(false);
        blackjackLabel.setVisible(false);
        pushLabel.setVisible(false);
        winningsLabel.setVisible(false);
        winningsLabel.setText("You Won: 0!");


        newCard6.setIcon(null);
        newCard6.setVisible(false);
        newCard5.setIcon(null);
        newCard5.setVisible(false);
        newCard4.setIcon(null);
        newCard4.setVisible(false);
        newCard3.setIcon(null);
        newCard3.setVisible(false);
        newCard2.setIcon(null);
        newCard2.setVisible(false);
        newCard.setIcon(null);
        newCard.setVisible(false);

        dealerCard1.setIcon(null);
        dealerCard1.setVisible(false);
        dealerCard2.setIcon(null);
        dealerCard2.setVisible(false);
        dealerCard3.setIcon(null);
        dealerCard3.setVisible(false);
        dealerCard4.setIcon(null);
        dealerCard4.setVisible(false);
        dealerCard5.setIcon(null);
        dealerCard5.setVisible(false);
        dealerCard6.setIcon(null);
        dealerCard6.setVisible(false);

        backContainer.repaint();
        obj.newDeal();
    }

    public void onHit() {
        obj.dealCard();
        ImageIcon hitCard = new ImageIcon(getClass().getResource("images/cards/%s.png".formatted(obj.currentCard)));
        obj.handCalc();
        setCounts();
        if (obj.handTotal > 21) {
            totalLabel.setText("Bust with: %s".formatted(obj.handTotal));
            loserLabel.setVisible(true);
            endHand();
        } else {
            totalLabel.setText("Hand Total is: %s".formatted(obj.handTotal));
        }

        if (newCard3.getIcon() == null) {
            newCard3.setIcon(hitCard);
            newCard3.setVisible(true);
        } else if (newCard4.getIcon() == null) {
            newCard4.setIcon(hitCard);
            newCard4.setVisible(true);
        } else if (newCard5.getIcon() == null) {
            newCard5.setIcon(hitCard);
            newCard5.setVisible(true);
        } else if (newCard6.getIcon() == null) {
            newCard6.setIcon(hitCard);
            newCard6.setVisible(true);
        }
    }

    public void mainGame() {
        createMain();
        cardButtons();
        betButtons();
        createLabels();

        this.setSize(1245, 680);
        this.setLocationRelativeTo(null);
        backContainer.removeAll();
        mainBackImage = new JLabel();
        mainBackImage.setLayout(null);
        mainBackImage.setSize(1245,680);
        ImageIcon table = new ImageIcon(getClass().getResource("images/blackjackBackground.png"));
        mainBackImage.setIcon(table);
        mainBackImage.setVisible(true);
        backContainer.add(mainBackImage);

        backContainer.add(newCard);
        backContainer.add(newCard2);
        backContainer.add(newCard3);
        backContainer.add(newCard4);
        backContainer.add(newCard5);
        backContainer.add(newCard6);

        backContainer.add(dealerCard1);
        backContainer.add(dealerCard2);
        backContainer.add(dealerCard3);
        backContainer.add(dealerCard4);
        backContainer.add(dealerCard5);
        backContainer.add(dealerCard6);

        backContainer.add(hit);
        backContainer.add(stand);
        backContainer.add(newHand);
        backContainer.add(deckStack);
        backContainer.add(countLabel);
        backContainer.add(trueCountLabel);
        backContainer.add(totalLabel);
        backContainer.add(dealerTotal);
        backContainer.add(winnerLabel);
        backContainer.add(loserLabel);
        backContainer.add(blackjackLabel);
        backContainer.add(pushLabel);
        backContainer.add(betLabel);
        backContainer.add(winningsLabel);
        backContainer.add(chipLabel);

        backContainer.add(betFive);
        backContainer.add(betQuarter);
        backContainer.add(betFifty);
        backContainer.add(betTfifty);
        backContainer.add(betReset);

        backContainer.setComponentZOrder(winnerLabel, 0);
        backContainer.setComponentZOrder(loserLabel, 1);
        backContainer.setComponentZOrder(blackjackLabel, 2);
        backContainer.setComponentZOrder(betLabel, 3);
        backContainer.setComponentZOrder(pushLabel, 4);
        backContainer.setComponentZOrder(winningsLabel, 5);

        backContainer.setComponentZOrder(newCard6, 6);
        backContainer.setComponentZOrder(newCard5, 7);
        backContainer.setComponentZOrder(newCard4, 8);
        backContainer.setComponentZOrder(newCard3, 9);
        backContainer.setComponentZOrder(newCard2, 10);
        backContainer.setComponentZOrder(newCard, 11);

        backContainer.setComponentZOrder(dealerCard6, 12);
        backContainer.setComponentZOrder(dealerCard5, 13);
        backContainer.setComponentZOrder(dealerCard4, 14);
        backContainer.setComponentZOrder(dealerCard3, 15);
        backContainer.setComponentZOrder(dealerCard2, 16);
        backContainer.setComponentZOrder(dealerCard1, 17);

        backContainer.setComponentZOrder(hit, 18);
        backContainer.setComponentZOrder(stand, 19);
        backContainer.setComponentZOrder(newHand, 20);
        backContainer.setComponentZOrder(deckStack, 21);
        backContainer.setComponentZOrder(countLabel, 22);
        backContainer.setComponentZOrder(totalLabel, 23);
        backContainer.setComponentZOrder(chipLabel, 24);
        backContainer.setComponentZOrder(dealerTotal, 25);

        backContainer.setComponentZOrder(betFive, 26);
        backContainer.setComponentZOrder(betQuarter, 27);
        backContainer.setComponentZOrder(betFifty, 28);
        backContainer.setComponentZOrder(betTfifty, 29);
        backContainer.setComponentZOrder(betReset, 30);
        backContainer.setComponentZOrder(trueCountLabel, 31);
        backContainer.setComponentZOrder(mainBackImage, 32);
        repaint();
    }

    public void dealStart() {
        clearTable();

        obj.burnCard();
        obj.dealerCards();
        obj.dealerAddBurn();
        obj.dealerHandCalc();
        ImageIcon dealerFirst = new ImageIcon(getClass().getResource("images/cards/%s.png".formatted(obj.dealersCurrentCard)));
        obj.dealCard();
        ImageIcon firstCard = new ImageIcon(getClass().getResource("images/cards/%s.png".formatted(obj.currentCard)));

        obj.dealerCards();
        dealerSecond = new ImageIcon(getClass().getResource("images/cards/%s.png".formatted(obj.dealersCurrentCard)));
        ImageIcon dealerHidden = new ImageIcon(getClass().getResource("images/cardBack.png"));
        obj.dealCard();
        ImageIcon SecondCard = new ImageIcon(getClass().getResource("images/cards/%s.png".formatted(obj.currentCard)));

        obj.handCalc();
        setCounts();
        checkBlackJack();
        totalLabel.setText("Hand Total is: %s".formatted(obj.handTotal));
        dealerTotal.setText("Dealer total: %s".formatted(obj.dealerHandTotal));


        dealerCard1.setIcon(dealerFirst);
        dealerCard1.setVisible(true);

        newCard.setIcon(firstCard);
        newCard.setVisible(true);

        dealerCard2.setIcon(dealerHidden);
        dealerCard2.setVisible(true);

        newCard2.setIcon(SecondCard);
        newCard2.setVisible(true);
        backContainer.repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buttonStart) {

            obj.createDeck();
            obj.createMainDeck();
            obj.shuffleDeck();
            cpuApple.startchips("easy");
            cpuBanano.startchips("easy");

            difficulty();

        } else if (e.getSource() == buttonExtras) {

            System.out.println("Do a cool trick here");

        } else if (e.getSource() == buttonEasy) {

            player.startchips("easy");
            player.findChip();

            mainGame();
            player.betSetting = 5;
            player.betPlaced = 5;
            player.subtractBet();
            chipLabel.setText("%s".formatted(player.chipTotal));
            dealStart();

        } else if (e.getSource() == buttonNormal) {

            player.startchips("normal");
            player.findChip();

            mainGame();
            player.betSetting = 5;
            player.betPlaced = 5;
            player.subtractBet();
            chipLabel.setText("%s".formatted(player.chipTotal));
            dealStart();

        } else if (e.getSource() == buttonHard) {

            player.startchips("hard");
            player.findChip();

            mainGame();
            player.betSetting = 5;
            player.betPlaced = 5;
            player.subtractBet();
            chipLabel.setText("%s".formatted(player.chipTotal));
            dealStart();

        } else if (e.getSource() == hit) {
            onHit();

        } else if (e.getSource() == stand) {

            endHand();
            turnDealer();
        } else if (e.getSource() == newHand) {
            if (obj.shuffledDeck.size() < 25) {
                obj = new Deck();
                obj.createDeck();
                obj.createMainDeck();
                obj.shuffleDeck();
                obj.softCount = 0;
                obj.trueCount = 0;
                obj.trueBurnPile.clear();
                obj.burnPile.clear();
            }
            player.placeBet();
            if (player.chipTotal - player.betPlaced < 0) {
                betLabel.setText("Not Enough Chips");
            } else if (player.betPlaced > 0) {
                player.subtractBet();
                chipLabel.setText("%s".formatted(player.chipTotal));

                dealStart();
            }
        } else if (e.getSource() == betFive) {
            betAdd(5);
        } else if (e.getSource() == betQuarter) {
            betAdd(25);
        } else if (e.getSource() == betFifty) {
            betAdd(50);
        } else if (e.getSource() == betTfifty) {
            betAdd(250);
        } else if (e.getSource() == betReset) {
            betAdd(0);
        }
    }

}
